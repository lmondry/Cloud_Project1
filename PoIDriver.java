package be_uclouvain_ingi2145_p1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.FileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.SimpleLayout;
import org.apache.log4j.varia.LevelRangeFilter;

public class PoIDriver extends Configured implements Tool
{
    /**
     * Singleton instance.
     */
    public static PoIDriver GET;

    /**
     * Author's name.
     */
    public static final String NAME = "MONDRY LAURENT";

    // ---------------------------------------------------------------------------------------------

    public static void main(String[] args) throws Exception
    {
        // configure log4j to output to a file
        Logger logger = LogManager.getRootLogger();
        logger.addAppender(new FileAppender(new SimpleLayout(), "p1.log"));

        // configure log4j to output to the console
        Appender consoleAppender = new ConsoleAppender(new SimpleLayout());
        LevelRangeFilter filter = new LevelRangeFilter();
        // switch to another level for more detail (own (INGI2145) messages use FATAL)
        filter.setLevelMin(Level.ERROR);
        consoleAppender.addFilter(filter);
        // (un)comment to (un)mute console output
        //logger.addAppender(consoleAppender);

        // switch to Level.DEBUG or Level.TRACE for more detail
        logger.setLevel(Level.INFO);

        GET = new PoIDriver();
        Configuration conf = new Configuration();

        int res = ToolRunner.run(conf, GET, args);
        System.exit(res);
    }

    // ---------------------------------------------------------------------------------------------

    // Depending on your implementation the number of stages might differ
    // Consider this skeleton as a outline you should be able to change it according to your needs.
    @Override
    public int run(String[] args) throws Exception
    {
        System.out.println(NAME);

        if (args.length == 0) {
            args = new String[]{ "command missing" };
        }

        switch (args[0]) {
            case "init":
                init(args[1], args[2], args[3], args[4], Integer.parseInt(args[5]));
                break;
            case "iter":
                iter(args[1], args[2], args[3], args[4], Integer.parseInt(args[5]), Integer.parseInt(args[6]));
                break;
            case "evaluate":
                evaluate(args[1], args[2], args[3], args[4], Integer.parseInt(args[5]));
                break;
            case "composite":
                composite(args[1], args[2], args[3], args[4], Integer.parseInt(args[5]), Integer.parseInt(args[6]));
                break;
            default:
                System.out.println("Unknown command: " + args[0]);
                break;
        }

        return 0;
    }

    // ---------------------------------------------------------------------------------------------

    /**
     *  Convert the initial format to the intermediate format
     */
    void init(String inputDir, String outputDir, String srcId, String dstId, int nReducers) throws Exception
    {
        Logger.getRootLogger().fatal("[INGI2145] init");

        // Deleting output directory if it exists
        Utils.deleteDir(outputDir);

        // Configuring the init job
        Job job = Utils.configureJob(inputDir,
        		outputDir,
        		MapInit.class,
        		null,
        		ReduceInit.class,
        		IntWritable.class,
        		Text.class,
        		IntWritable.class,
        		Text.class,
        		nReducers);

        // Adding destination ID and source ID to the job configuration to access them inside the MapReduce for that job.
		job.getConfiguration().setInt("srcId", Integer.parseInt(srcId));
		job.getConfiguration().setInt("dstId", Integer.parseInt(dstId));

		// Starting the job
    	try{
    		Utils.startJob(job);
    	}catch(Exception e){
    		System.err.println("Init failed");
    		System.exit(-1);
    	}
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Perform a single round of the Person of Interest algorithm
     */
    void iter(String inputDir, String outputDir, String srcId, String dstId, int iterNo, int nReducers) throws Exception //iterNo is the value of iteration counter
    {
        Logger.getRootLogger().fatal("[INGI2145] iter: " + inputDir + " (to) " + outputDir);

        // Deleting output directory if it exists
        Utils.deleteDir(outputDir);

        // Configuring the iter job
        Job job = Utils.configureJob(inputDir,
        		outputDir,
        		MapIter.class,
        		null,
        		ReduceIter.class,
        		IntWritable.class,
        		Text.class,
        		IntWritable.class,
        		Text.class,
        		nReducers);

        // Adding current iteration, destination ID and source ID to the job configuration to access them inside the MapReduce for that job.
		job.getConfiguration().setInt("srcId", Integer.parseInt(srcId));
		job.getConfiguration().setInt("dstId", Integer.parseInt(dstId));
		job.getConfiguration().setInt("currentIter", iterNo);

		// Starting the job
    	try{
    		Utils.startJob(job);
    	}catch(Exception e){
    		System.err.println("Iter failed");
    		System.exit(-1);
    	}
    }


    // ---------------------------------------------------------------------------------------------

    /**
     * Evaluate check if the algorithm should terminate or not
     */
    void evaluate(String inputDir, String outputDir, String srcId, String dstId, int nReducers) throws Exception
    {
        Logger.getRootLogger().fatal("[INGI2145] evaluate from:" + inputDir);

        // Deleting output directory if it exists
        Utils.deleteDir(outputDir);

        // Configuring the evaluate job
        Job job = Utils.configureJob(inputDir,
        		outputDir,
        		MapEvaluate.class,
        		null,
        		ReduceEvaluate.class,
        		IntWritable.class,
        		IntWritable.class,
        		IntWritable.class,
        		Text.class,
        		nReducers);

        // Adding destination ID and source ID to the job configuration to access them inside the MapReduce for that job.
		job.getConfiguration().setInt("srcId", Integer.parseInt(srcId));
		job.getConfiguration().setInt("dstId", Integer.parseInt(dstId));

		// Starting the job
    	try{
    		Utils.startJob(job);
    	}catch(Exception e){
    		System.err.println("Evaluate failed");
    		System.exit(-1);
    	}
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Perfom the whole algorithm starting from init and then making one loop that executes iter and evaluate successively
     */
    void composite(String inputDir, String outputDir, String srcId, String dstId, int maxHops, int nReducers) throws Exception // maxHops is the maximum number of hops in which the destination should be reached
    {
        Logger.getRootLogger().fatal("[INGI2145] composite: " + inputDir + " (to) " + outputDir);

        // Deleting output directory if it exists
        Utils.deleteDir(outputDir);

        // Creating temporary directory to store intermediate results
        String tmpDir = "tmp0";
        String src = "";
        String dst = "";

        // Launching the init job
        init(inputDir, tmpDir, srcId, dstId, nReducers);

        // Looping through iter and init until maxhops or until checkResults == true
        for(int i = 0;i<maxHops;i++){
        	// Given an output folder, check if a connection node has been found for the Person of Interest
        	if(!Utils.checkResults(null, new Path(outputDir))){
        		// Iteratively naming temporary directory
        		src = "tmp"+i;
            	dst = "tmp"+(i+1);
            	// Launching one iter job
            	iter(src,dst,srcId,dstId,i,nReducers);
            	// Launching one evaluate job
            	evaluate(dst,outputDir,srcId,dstId,nReducers);
        	}
        }
        if(!Utils.checkResults(null, new Path(outputDir))){
        	writeNull(inputDir, outputDir, nReducers);
        }
    }

    /**
     * That job only write "1 []" to the output file
     */
    void writeNull(String inputDir, String outputDir, int nReducers) throws Exception
    {
    	// Deleting output directory if it exists
        Utils.deleteDir(outputDir);

        // Configuring the evaluate job
        Job job = Utils.configureJob(inputDir, // The inputDir here is not important, it just have to contains one line to be considered by the mapper and the reducer
        		outputDir,
        		MapNull.class,
        		null,
        		ReduceNull.class,
        		IntWritable.class,
        		IntWritable.class,
        		IntWritable.class,
        		Text.class,
        		nReducers);

		// Starting the job
    	try{
    		Utils.startJob(job);
    	}catch(Exception e){
    		System.err.println("Write null failed");
    		System.exit(-1);
    	}
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Each line of the init file contains a edge. Each edge is converted into two nodes.
     * Inside these nodes, each endpoint contains the id the the other endpoint.
     */
    public static class MapInit extends Mapper<LongWritable, Text, IntWritable, Text>
    {
        @Override
        protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException
        {
        	Text token = new Text();
            String line = value.toString();
            StringTokenizer tokenizer = new StringTokenizer(line);

            // Each list is going to contain the other endpoint
            // I'm using an arraylist instead of an int because the node constructor is Node(int id, ArrayList<Integer> neighbors)
            ArrayList<Integer> list1 = new ArrayList<Integer>();
            ArrayList<Integer> list2 = new ArrayList<Integer>();

            while (tokenizer.hasMoreTokens()) {
            	// This token is the id for the first node and the neighbor for the second
            	token.set(tokenizer.nextToken());
                int id1 = Integer.parseInt(token.toString());
                int neighbor2 = Integer.parseInt(token.toString());
                list2.add(neighbor2);

                // This token is the id for the second node and the id for the first
                token.set(tokenizer.nextToken());
                int id2 = Integer.parseInt(token.toString());
                int neighbor1 = Integer.parseInt(token.toString());
                list1.add(neighbor1);

                // Instantiating to Node objects
            	Node node1 = new Node(id1,list1);
            	Node node2 = new Node(id2,list2);

            	// Write these two nodes to send them to the reducer
                context.write(new IntWritable(id1), node1.toText());
                context.write(new IntWritable(id2), node2.toText());
            }
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * The reduce is going to take all the neighbors for one node merge them to one ArrayList of neighbors
     */
    public static class ReduceInit extends Reducer<IntWritable, Text, IntWritable, Text>
    {
    	@Override
        protected void reduce(IntWritable key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException
        {
    		// ArrayList that contains all the neighbors for one node
    		ArrayList<Integer> list = new ArrayList<Integer>();

    		// Iterate through the values send to the reducer from the mapper
    		for(Text value : values){
    			// With each line we'll create a temporary node
    			Node tmp = new Node(key.get(),value.toString());

    			// Merging the neighbors of the temporary node inside the main arraylist
    			for(int neighbor : tmp.getNeighbor()){
    				list.add(neighbor);
    			}
    		}

    		// Creating a new node containing the merged neighbors list
    		Node node = new Node(key.get(),list);
    		// Check if the current node is the source received in parameters
    		node.setSource(context.getConfiguration().getInt("srcId", 0));
    		// Write the result in the intermediate format
    		context.write(key,node.toText());

        }
    }

// ---------------------------------------------------------------------------------------------

    /**
     * Parse the intermediate file, create the needed nodes (current and neighbor of that current node) and send them to the reducer
     */
    public static class MapIter extends Mapper<LongWritable, Text, IntWritable, Text>
    {
        @Override
        protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException
        {
        	// Creating a new node for each line in the intermediate file
        	Node node = new Node(value.toString());

        	// Updating the iter value for the current node
        	node.setIter(context.getConfiguration().getInt("currentIter", 0));

        	// If the node is going to be visited (=> if the node.getVisited() == 1)
        	if(node.getVisited() == 1){

        		// I change the visited value from "to visit" (=1) to "visited" (=2)
        		// (if visited == 0, that means that the node won't be visited this iteration)
        		node.setVisited(2);

        		// I wrote the current node without making any more change
        		context.write(new IntWritable(node.getId()), node.toText());

        		// I create a new node for each neighbor of the current node
        		for(int neighbor : node.getNeighbor()){
            		ArrayList<Integer> list = new ArrayList<Integer>();
            		// Each neighbor is going to have the current node as neighbor
            		list.add(node.getId());
            		// Creating the node
            		Node tmp = new Node(neighbor,list);
            		// Each neighbor of the current node are going to be visited on the next iter so I changed their visited value to 1
            		tmp.setVisited(1);
            		// Each neighbor get the current iter value
            		tmp.setIter(context.getConfiguration().getInt("currentIter", 0));
            		// I wrote send each neighbor to the Reducer
            		context.write(new IntWritable(neighbor),tmp.toText());
            	}
        	}else{
        		// If the node isn't going to be visited on the next iteration (visited = 2 => already visited no need to revisit, visited = 0 => not going to be visited on the next iter)
        		context.write(new IntWritable(node.getId()), node.toText());
        	}
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * The reducer will merge every nodes with the same ID
     * When merging, it'll take the max node.visited value to avoid visiting the same node twice
     */
    public static class ReduceIter extends Reducer<IntWritable, Text, IntWritable, Text>
    {
    	@Override
        protected void reduce(IntWritable key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException
        {
    		// The set is used to avoid having two same neighbor in one node
    		Set<Integer> set = new HashSet<Integer>();
    		// The ArrayList is used to store each neighbor to create the node
    		ArrayList<Integer> list = new ArrayList<Integer>();
    		// Max value of node.visited found
    		int maxVisited = 0;

    		// Iterating through each received node
    		for(Text value : values){
    			// Creating a new temporary node
    			Node tmp = new Node(key.get(),value.toString());
    			// Iterating through each neighbor the the node being evaluated
    			for(int neighbor : tmp.getNeighbor()){
    				if (! set.contains(neighbor)){
    					// Adding neighbor to the set if not in it
    					set.add(neighbor);
    				}
    			}
    			// Getting the max node.visited value
    			if(tmp.getVisited()>maxVisited){
    				maxVisited = tmp.getVisited();
    			}
    		}

    		// Adding neighbor to the arraylist
    		for(int neighbor : set){
    			list.add(neighbor);
    		}

    		// Creating the node to be written
    		Node node = new Node(key.get(),list);
    		node.setVisited(maxVisited);
    		node.setIter(context.getConfiguration().getInt("currentIter", 0));

    		// Writing it to the intermediate file
    		context.write(key,node.toText());
        }
    }

// ---------------------------------------------------------------------------------------------

    /**
     * We are checking all the nodes that are going to be evaluated in the next iter, and if they're going to and if in their neighbors there is the destination node, we write them
     */
    public static class MapEvaluate extends Mapper<LongWritable, Text, IntWritable, IntWritable>
    {
        @Override
        protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException
        {
        	// Creating a new node from the value received
        	Node node = new Node(value.toString());

        	// If the node is going to be visited on the next iter
        	if(node.getVisited() == 1){
        		// If the current node have the dstId as the neighbor
        		if(node.getNeighbor().contains(context.getConfiguration().getInt("dstId", 0))){
        			// Send the iter value and the node id to the reducer
        			context.write(new IntWritable(node.getIter()), new IntWritable(node.getId()));
        		}
        	}
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Takes all the ids received from the mapper and write the output file in the right format
     */
    public static class ReduceEvaluate extends Reducer<IntWritable, IntWritable, IntWritable, Text>
    {
    	@Override
        protected void reduce(IntWritable key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException
        {
    		StringBuffer buffer = new StringBuffer();
    		Boolean first = true;

    		buffer.append("[");
    		for(IntWritable value : values){
    			if(first){
    				buffer.append(value.toString());
    				first = false;
    			}else{
    				buffer.append(",").append(value.toString());
    			}
    		}
    		buffer.append("]");

    		context.write(key, new Text(buffer.toString()));
		}
	}

 // ---------------------------------------------------------------------------------------------

    /**
   	 * I send one key and one value to the reducer
     */
    public static class MapNull extends Mapper<LongWritable, Text, IntWritable, IntWritable>
    {
        @Override
        protected void map(LongWritable key, Text value, Context context)
            throws IOException, InterruptedException
        {
        	context.write(new IntWritable(1), new IntWritable(1));
        }
    }

    // ---------------------------------------------------------------------------------------------

    /**
     * Takes all the ids received from the mapper and write the output file in the right format
     */
    public static class ReduceNull extends Reducer<IntWritable, IntWritable, IntWritable, Text>
    {
    	@Override
        protected void reduce(IntWritable key, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException
        {
    		StringBuffer buffer = new StringBuffer();

    		buffer.append("[]");

    		// Write the key received, which is 1, and the String [] to the output file
    		context.write(key, new Text(buffer.toString()));
		}
	}

 // ---------------------------------------------------------------------------------------------

    /**
     * Class node :
     * - Parse the received values
     * - Create node
     */
    public static class Node
    {
    	private int id = -1;					//id of the node
    	private int visited = 0; 				//0 unexplored, 1 will be explored, 2 explored
    	private int iter = 0;					//current iter value
    	private ArrayList<Integer> neighbors; 	//list of neighbors

    	// Constructor used to create a node from an id and an arraylist of neighbor
    	public Node(int id, ArrayList<Integer> neighbors){
    		this.id = id;
    		this.neighbors = neighbors;
    	}

    	// Constructor used to create a node from a line in the intermediate format received by the Mapper by parsing it
    	public Node(String line){
    		neighbors = new ArrayList<Integer>();
    		String[] input = line.split("\t");
    		String infos = "";

    		id = Integer.valueOf(input[0]);
    		infos = input[1];

    		String[] values = infos.split("\\|");

    		visited = Integer.valueOf(values[0]);

    		iter = Integer.valueOf(values[1]);

    		String[] neighNodes = values[2].split(",");

    		for(String neighNode: neighNodes){
    			neighbors.add(Integer.parseInt(neighNode));
    		}
    	}

    	// Constructor used to create a node from a line in the intermediate format received by the Reducer parsing it
    	public Node(int id, String line){
    		this.id = id;
    		neighbors = new ArrayList<Integer>();

    		String[] values = line.split("\\|");

    		visited = Integer.valueOf(values[0]);

    		iter = Integer.valueOf(values[1]);

    		String[] neighNodes = values[2].split(",");

    		for(String neighNode: neighNodes){
    			neighbors.add(Integer.parseInt(neighNode));
    		}
    	}

    	// This function is used to determinate if the current node is the source of the graph
    	public void setSource(int source){
    		if(source == id){
    			visited = 1;
    		}else{
    			visited = 0;
    		}
    	}

    	// This function is used to convert the node object to a Hadoop Text
    	public Text toText(){
    		StringBuffer buffer = new StringBuffer();
    		Boolean first = true;

    		buffer.append(visited).append("|");
    		buffer.append(iter).append("|");
    		for(Integer neighbor : neighbors){
    			if (first){
    				buffer.append(neighbor);
    				first = false;
    			}else{
    				buffer.append(",").append(neighbor);
    			}
    		}

			return new Text(buffer.toString());
    	}

    	//--------------------
    	// Getters and Setters
    	public void setId(int id){
    		this.id = id;
    	}
    	public int getId(){
    		return id;
    	}
    	public void setNeighbor(ArrayList<Integer> neighbors){
    		this.neighbors = neighbors;
    	}
    	public ArrayList<Integer> getNeighbor(){
    		return neighbors;
    	}
    	public void setVisited(int i){
    		this.visited = i;
    	}
    	public int getVisited(){
    		return visited;
    	}
    	public int getIter(){
    		return iter;
    	}
    	public void setIter(int i){
    		this.iter = i;
    	}
    	//--------------------
    }
}
